'use strict';
var gulp = require('gulp'),
    util = require('gulp-util'),
    sass = require('gulp-sass'),
    sassGlob = require('gulp-sass-glob'),
    concat = require('gulp-concat'),
    concatCss = require('gulp-concat-css'),
    uglify = require('gulp-uglify'),
    cleanCSS = require('gulp-clean-css'),
    watch = require('gulp-watch'),
    jshint = require('gulp-jshint'),
    notify = require("gulp-notify"),
    autoprefixer = require('gulp-autoprefixer');

/*
Edit project name and feel free to edit input outputs
 */
var config = {
    projectName: 'lab',
    sassDir: './src/sass/**/*.scss',
    cssDir: ['dist/css/vendor.css', 'dist/css/labs.css'],
    vendorCss: 'src/vendor/css/**/*.css',
    cssOutput: 'dist/css',
    jsDir: ['dist/js/vendor.js', 'dist/js/scripts.js'],
    jsFunctionsDir: 'src/js/scripts/*.js',
    vendorJs: ['src/vendor/js/lib/*.js', 'src/vendor/js/plugins/*.js'],
    jsOutput: 'dist/js/',
    production: !!util.env.production
    };

gulp.task('compileCss', function () {
    return gulp.src(config.sassDir).pipe(sassGlob()).pipe(sass({
        style: 'expanded',
        sourceComments: 'normal'
    }).on('error', sass.logError))
        .pipe(autoprefixer())
        .pipe(gulp.dest(config.cssOutput)).pipe(notify({
            message: 'SASS compilation complete',
            onLast: true
        }));
});
gulp.task('compileVendorJs', function () {
    return gulp.src(config.vendorJs)
        .pipe(concat('vendor.js'))
        .pipe(gulp.dest(config.jsOutput)).pipe(notify({
        message: 'Vendor JS concat complete',
        onLast: true
    }));
});
gulp.task('compileVendorCss', function () {
    return gulp.src(config.vendorCss)
        .pipe(concatCss("/vendor.css"))
        .pipe(gulp.dest(config.cssOutput)).pipe(notify({
        message: 'Vendor CSS compilation complete',
        onLast: true
    }));
});

gulp.task('compileJs', function (done) {
    return gulp.src(config.jsFunctionsDir)
        .pipe(jshint())
        .pipe(jshint.reporter('jshint-stylish'))
        .pipe(jshint.reporter('fail'))
        .pipe(concat('scripts.js'))
        .pipe(gulp.dest(config.jsOutput))
        .pipe(notify("JS compilation complete"), done);
});

gulp.task('concatJs', ['compileJs', 'compileVendorJs'], function () {
    return gulp.src(config.jsDir)
        .pipe(concat(''+config.projectName+'.js'))
        //Only compress if --production passed in task
        .pipe(config.production ? uglify() : util.noop())
        .pipe(gulp.dest(config.jsOutput)).pipe(notify({
            message: 'JS Concat complete',
            onLast: true 
        }));
});

gulp.task('concatCss', ['compileCss', 'compileVendorCss'], function () {

    return gulp.src(config.cssDir)
        .pipe(concatCss(''+config.projectName+'.css'))
        //Only compress if --production passed in task
        .pipe(config.production ? cleanCSS({compatibility: 'ie8'}) : util.noop())
        .pipe(gulp.dest('dist/css/')).pipe(notify({
            message: 'CSS concat complete',
            onLast: true
        }));
});

gulp.task('default', ['watch']);
//Task to compile everything just thrown in just in case its needed
gulp.task('compileAll', ['compileCss', 'compileVendorCss', 'concatCss','compileJs', 'compileVendorJs', 'concatJs']);

gulp.task('watch', function () {
    gulp.watch(['src/sass/**/*.scss', 'src/vendor/css/**/*.css'], ['compileCss', 'compileVendorCss', 'concatCss']);
    gulp.watch(['src/js/**/*.js', 'src/vendor/js/**/*.js'], ['compileJs', 'compileVendorJs', 'concatJs'])
});