# Gulp template #

add something

Gulp file with lib sass and modular based javascript development idea

### How do I get set up? ###

Just run npm install and all dependencies will be installed

* Default task is Watch just run Gulp to get going

* Logic added for production mode which will minify Js and CSS on compilation, to enable production mode add --production to the end of any task 

# Avaliable tasks #
* watch ( Watches Sass, and Js files and runs tasks depending on what file is edited)

* compileAll (This will run through and compile all CSS and JS without watching for further changes)

### tasks which are run by compileAll of Watch ###
* compileCss
* compileVendorCss
* concatCss
* compileJs
* compileVendorJs
* concatJs

### Whats the crack ###

* JS functions are written on a file basis gulp tasks compile all functions into 1 file
* Jshint will halt gulp watch or not allow js compilaiton if an error is found ( Save the console errors)
* Sass compiled with Lib Sass 
